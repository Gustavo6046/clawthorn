// Bitmap manipulation.

#ifndef HG_BITMAP
#define HG_BITMAP

#include <stdlib.h>
#include <stdio.h>

struct ct_rs_Color_s;



typedef struct ct_rb_DoubleBitmap_s {
    long width;
    long height;
    double *vals;
} ct_rb_DoubleBitmap;

typedef struct ct_rb_RGBBitmap_s {
    long width;
    long height;
    double *vals;
} ct_rb_RGBBitmap;
// we could do typedef ct_rb_DoubleBitmap to ct_rb_RGBBitmap, but this
// is more typesafe :)


ct_rb_DoubleBitmap *ct_rb_newD(short width, long height);
ct_rb_RGBBitmap *ct_rb_newRGB(short width, long height);
ct_rb_RGBBitmap *ct_rb_pngLoadRGB(FILE *fp, char *nameForTroubleshooting);

void ct_rb_clearGrey(ct_rb_DoubleBitmap *bitmap);
void ct_rb_clearRGB(ct_rb_RGBBitmap *bitmap);

void ct_rb_freeD(ct_rb_DoubleBitmap *bitmap);
void ct_rb_freeRGB(ct_rb_RGBBitmap *bitmap);

void ct_rb_exportGreyscale(FILE *fp, ct_rb_DoubleBitmap *bitmap);
void ct_rb_exportRGB(FILE *fp, ct_rb_RGBBitmap *bitmap);

void ct_rb_setpix_rgb(ct_rb_RGBBitmap *bitmap, long x, long y, struct ct_rs_Color_s *col);
void ct_rb_getpix_rgb(ct_rb_RGBBitmap *bitmap, long x, long y, struct ct_rs_Color_s *col);


#include "surface.h"



#endif
