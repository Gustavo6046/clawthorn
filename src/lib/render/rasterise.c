#include <math.h>
#include <assert.h>
#include <string.h>
#include <stdint.h>

#include "rasterise.h"
#include "surface.h"
#include "lib/common/vector.h"
#include "lib/common/fixed.h"



void ct_rr_clearDepths(ct_rr_Rasterisation *targ) {
    for (uint32_t i = 0; i < targ->surf->width * targ->surf->height; i++) {
        targ->blockDepths[i] = 0.;
    }
}


long convertX(ct_rr_Rasterisation *targ, double x) {
    return floor((x + 1) / 2 * targ->bigside);
}

long convertY(ct_rr_Rasterisation *targ, double y) {
    return floor((y + 1) / 2 * targ->bigside);
}

double unconvertX(ct_rr_Rasterisation *targ, long x) {
    return (double)(x) * 2 / targ->bigside - 1;
}

double unconvertY(ct_rr_Rasterisation *targ, long y) {
    return (double)(y) * 2 / targ->bigside - 1;
}


void ct_rr_applyPixel(ct_rr_Rasterisation *targ, ct_ct_Triangle2D *tri, double x, double y, double p_x, double p_y, double bary_a, double bary_b, double bary_c, double depth, ct_rs_Color *targCol) {
    for (uint8_t sh_i = 0; sh_i < tri->numShaders; sh_i++) {
        ct_rh_Shader *shader = (ct_rh_Shader *) tri->shaders[sh_i];

        ct_rs_Color col = (*shader->source->colorFor)(shader->source, tri, x, y, p_x, p_y, bary_a, bary_b, bary_c, depth);
        (*shader->applier->apply)(shader->applier, x, y, p_x, p_y, &col, targCol);
    }
}


char ct_rr_rasterizePixel(ct_rr_Rasterisation *targ, ct_ct_Triangle2D *tri, double bary_a, double bary_b, double bary_c, long x, long y, double p_x, double p_y) {
    double depth = FXTOF(tri->a.z) * bary_a + FXTOF(tri->b.z) * bary_b + FXTOF(tri->c.z) * bary_c;

    // depth check

    if (depth > 0 && depth < 1) {
        long di = y * targ->surf->width + x;
        double orig_depth = targ->blockDepths[di];

        if (orig_depth == 0. || depth < orig_depth) {
            ct_rs_Color targCol = (*targ->surf->getPixelValue)(targ->surf, x, y);

            ct_rr_applyPixel(targ, tri, x, y, p_x, p_y, bary_a, bary_b, bary_c, depth, &targCol);
            (*targ->surf->setPixel)(targ->surf, x, y, &targCol);

            targ->blockDepths[di] = depth;

            return 1;
        }
    }

    return 0;
}


void ct_rr_init_raster(ct_rr_Rasterisation *targ, ct_rs_Surface *surf) {
    targ->surf = surf;

    targ->bigside = surf->width;
    if (surf->height > targ->bigside) targ->bigside = surf->height;

    targ->blockDepths = malloc(sizeof(double) * surf->width * surf->height);
    ct_rr_clearDepths(targ);
}

void ct_rr_deinit_raster(ct_rr_Rasterisation *raster) {
    free(raster->blockDepths);
}

void ct_rr_free_raster(ct_rr_Rasterisation *raster) {
    ct_rr_deinit_raster(raster);
    free(raster);
}

static void capCoordX(long *x, ct_rs_Surface *surf) {
    if (*x < 0) *x = 0;
    if (surf && *x > surf->width) *x = surf->width;
}

static void capCoordY(long *y, ct_rs_Surface *surf) {
    if (*y < 0) *y = 0;
    if (surf && *y > surf->height) *y = surf->height;
}

long ct_rr_rasterizeInto(ct_rr_Rasterisation *targ, ct_ct_Triangle2D *tri) {
    // Find boundaries
    double                      minX = tri->a.x;
    if (tri->b.x < minX)        minX = tri->b.x;
    if (tri->c.x < minX)        minX = tri->c.x;
    if (-1 > minX)              minX = -1;
    if ( 1 < minX)              minX = 1;

    double                      minY = tri->a.y;
    if (tri->b.y < minY)        minY = tri->b.y;
    if (tri->c.y < minY)        minY = tri->c.y;
    if (-1 > minY)              minY = -1;
    if ( 1 < minY)              minY = 1;

    double                      maxX = tri->a.x;
    if (tri->b.x > maxX)        maxX = tri->b.x;
    if (tri->c.x > maxX)        maxX = tri->c.x;
    if (-1 > maxX)              maxX = -1;
    if ( 1 < maxX)              maxX = 1;

    double                      maxY = tri->a.y;
    if (tri->b.y > maxY)        maxY = tri->b.y;
    if (tri->c.y > maxY)        maxY = tri->c.y;
    if (-1 > maxY)              maxY = -1;
    if ( 1 < maxY)              maxY = 1;

    long pMinX = convertX(targ, minX);
    long pMaxX = convertX(targ, maxX);
    
    long pMinY = convertY(targ, minY);
    long pMaxY = convertY(targ, maxY);

    capCoordX(&pMinX, targ->surf);
    capCoordX(&pMaxX, targ->surf);
    capCoordY(&pMinY, targ->surf);
    capCoordY(&pMaxY, targ->surf);

    assert(pMinX <= pMaxX);
    assert(pMinY <= pMaxY);

    if (pMinX == pMaxX && pMinY == pMaxY) return 0; // invalid bounding box

    long pWidth    = pMaxX - pMinX;
    long pHeight   = pMaxY - pMinY;
    long pArea     = pWidth * pHeight;

    if (pArea == 0) return 0; // zero area bounding box

    // Find barycentric slopes
    ct_cv_Vector edge_ab, edge_ac;
    ct_cv_setSub(&edge_ab, &tri->b, &tri->a);
    ct_cv_setSub(&edge_ac, &tri->c, &tri->a);

    double regCross = ct_cv_cross2(&edge_ab, &edge_ac);

    if (regCross == 0) return 0; // zero area triangle
    
    double edge_ap_l  = -1. - tri->a.x;
    double edge_ap_r  =  1. - tri->a.x;
    double edge_ap_b  = -1. - tri->a.y;
    double edge_ap_t  =  1. - tri->a.y;
    double edge_ap_cx =     - tri->a.x;
    double edge_ap_cy =     - tri->a.y;

    ct_cv_Vector p_tl = ct_cv_vVal(edge_ap_l, edge_ap_t, 0);
    ct_cv_Vector p_bl = ct_cv_vVal(edge_ap_l, edge_ap_b, 0);
    ct_cv_Vector p_br = ct_cv_vVal(edge_ap_r, edge_ap_b, 0);
    ct_cv_Vector p_cn = ct_cv_vVal(edge_ap_cx, edge_ap_cy, 0);

    double bary_a_tl = ct_cv_cross2f(&p_tl, &edge_ac) / regCross;
    double bary_a_bl = ct_cv_cross2f(&p_bl, &edge_ac) / regCross;
    double bary_a_br = ct_cv_cross2f(&p_br, &edge_ac) / regCross;
    double bary_a_cn = ct_cv_cross2f(&p_cn, &edge_ac) / regCross;

    double bary_b_tl = ct_cv_cross2f(&edge_ab, &p_tl) / regCross;
    double bary_b_bl = ct_cv_cross2f(&edge_ab, &p_bl) / regCross;
    double bary_b_br = ct_cv_cross2f(&edge_ab, &p_br) / regCross;
    double bary_b_cn = ct_cv_cross2f(&edge_ab, &p_cn) / regCross;

    /*
    double bary_a_top_lf = (edge_ap_l * edge_ac.y - edge_ap_t * edge_ac.x) / regCross;
    double bary_a_bot_lf = (edge_ap_l * edge_ac.y - edge_ap_b * edge_ac.x) / regCross;
    double bary_a_bot_rt = (edge_ap_r * edge_ac.y - edge_ap_b * edge_ac.x) / regCross;

    double bary_b_top_lf = (edge_ab.x * edge_ap_t - edge_ab.y * edge_ap_l) / regCross;
    double bary_b_bot_lf = (edge_ab.x * edge_ap_b - edge_ab.y * edge_ap_l) / regCross;
    double bary_b_bot_rt = (edge_ab.x * edge_ap_b - edge_ab.y * edge_ap_r) / regCross;
    */

    double bary_a_slp_x = (bary_a_br - bary_a_bl) / 2;
    double bary_a_slp_y = (bary_a_tl - bary_a_bl) / 2;

    double bary_b_slp_x = (bary_b_br - bary_b_bl) / 2;
    double bary_b_slp_y = (bary_b_tl - bary_b_bl) / 2;

    double bary_a_int = bary_a_cn;
    double bary_b_int = bary_b_cn;

    // Paint pixels

    char side_case = -1;

    long setPix = 0;

    for (long y = pMinY; y <= pMaxY && y < targ->surf->height; y++) {
        // per row
        double p_y = unconvertY(targ, y);

        // ...find scanline edges
        long x1 = pMinX;
        long x2 = pMaxX;

        capCoordX(&x1, targ->surf);
        capCoordX(&x2, targ->surf);

        double p_x1 = unconvertX(targ, x1);
        double p_x2 = unconvertX(targ, x2);

        // compute barycentric coordinates at scanline edges
        double bary_a_x1 = bary_a_slp_x * p_x1 + bary_a_slp_y * p_y + bary_a_int;
        double bary_b_x1 = bary_b_slp_x * p_x1 + bary_b_slp_y * p_y + bary_b_int;
        double bary_c_x1 = 1. - bary_a_x1 - bary_b_x1;

        double bary_a_x2 = bary_a_slp_x * p_x2 + bary_a_slp_y * p_y + bary_a_int;
        double bary_b_x2 = bary_b_slp_x * p_x2 + bary_b_slp_y * p_y + bary_b_int;
        double bary_c_x2 = 1. - bary_a_x1 - bary_b_x1;

        // iterate on each pixel of scanline
        for (long x = pMinX; x <= pMaxX && x < targ->surf->width; x++) {
            // find interpolation ratio
            double ratio = (double)(pMaxX - pMinX) / (x - pMinX);

            // interpolate p_x and barycentric coordinates
            double p_x = p_x1 + (p_x2 - p_x1) * ratio;

            double bary_a = bary_a_x1 + (bary_a_x2 - bary_a_x1) * ratio;
            double bary_b = bary_b_x1 + (bary_b_x2 - bary_b_x1) * ratio;
            double bary_c = bary_c_x1 + (bary_c_x2 - bary_c_x1) * ratio;

            if (side_case != 1) {
                if (bary_a > 0 && bary_b > 0 && bary_c > 0) {
                    setPix += ct_rr_rasterizePixel(targ, tri, bary_a, bary_b, bary_c, x, y, p_x, p_y);
                }

                side_case = 0;
            }

            if (side_case != 0 && !setPix) {
                bary_a *= -1;
                bary_b *= -1;
                bary_c  =  1. - bary_a - bary_b;

                if (bary_a > 0 && bary_b > 0 && bary_c > 0) {
                    setPix += ct_rr_rasterizePixel(targ, tri, bary_a, bary_b, bary_c, x, y, p_x, p_y);
                }

                side_case = 1;
            }
        }

        continue;

        /* -- older, slower per-pixel code. do not use
        for (long x = pMinX; x <= pMaxX && x < targ->surf->width; x++) {
            double p_x = unconvertX(targ, x);
            double p_y = unconvertY(targ, y);

            /-*
            we won't use neither

                ct_cv_Vector *p = ct_cv_vNew(p_x, p_y, 0);
                ct_cv_Vector *edge_ap = ct_cv_sub(p, tri->a);

            nor

                double edge_ap_x = p_x - tri->a.x;
                double edge_ap_y = p_y - tri->a.y;

            because we don't need
            
                double bary_a = ct_cv_cross2(edge_ap, edge_ac) / regCross;
                double bary_b = ct_cv_cross2(edge_ab, edge_ap) / regCross;
            *-/

            double bary_a = bary_a_slp_x * p_x + bary_a_slp_y * p_y + bary_a_int;
            double bary_b = bary_b_slp_x * p_x + bary_b_slp_y * p_y + bary_b_int;
            double bary_c = 1. - bary_a - bary_b;

            if (side_case != 1) {
                if (bary_a > 0 && bary_b > 0 && bary_c > 0) {
                    setPix += ct_rr_rasterizePixel(targ, tri, bary_a, bary_b, bary_c, x, y, p_x, p_y);
                }

                side_case = 0;
            }

            if (side_case != 0 && !setPix) {
                bary_a *= -1;
                bary_b *= -1;
                bary_c  =  1. - bary_a - bary_b;

                if (bary_a > 0 && bary_b > 0 && bary_c > 0) {
                    setPix += ct_rr_rasterizePixel(targ, tri, bary_a, bary_b, bary_c, x, y, p_x, p_y);
                }

                side_case = 1;
            }

            //ct_cv_vFree(edge_ap);
            //ct_cv_vFree(p);
        }
        */
    }

    //-- variables moved to stack, don't uncomment as there are no memory leaks now
    //ct_cv_vFree(edge_ab);
    //ct_cv_vFree(edge_ac);

    return setPix;
}

