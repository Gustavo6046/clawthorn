#ifndef HG_WINDOW
#define HG_WINDOW

#include <SDL2/SDL.h>


struct ct_rb_DoubleBitmap_s;
struct ct_rb_RGBBitmap_s;

typedef struct {
    SDL_Renderer                 *renderer;
    SDL_Window                   *window;
    struct ct_rb_RGBBitmap_s     *pixelBuffer;

} ct_rw_SDLApp;

ct_rw_SDLApp *ct_rw_new(const char *title, int width, int height, int renderFlags, int windowFlags);
void ct_rw_init(ct_rw_SDLApp *app, const char *title, int width, int height, int renderFlags, int windowFlags);
void ct_rw_deinit(ct_rw_SDLApp *app);
void ct_rw_renderBitmapRGB(ct_rw_SDLApp *app, struct ct_rb_RGBBitmap_s *bitmap);
void ct_rw_renderBitmapG(ct_rw_SDLApp *app, struct ct_rb_DoubleBitmap_s *bitmap);
void ct_rw_rendererPresent(ct_rw_SDLApp *app);
void ct_rw_clearColor(ct_rw_SDLApp *app);

#include "bitmap.h"



#endif
