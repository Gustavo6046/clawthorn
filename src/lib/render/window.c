#include <SDL2/SDL.h>

#include "limits.h"
#include "window.h"
#include "lib/common/abort.h"



ct_rw_SDLApp *ct_rw_new(const char *title, int width, int height, int renderFlags, int windowFlags) {
    ct_rw_SDLApp *app = malloc(sizeof(ct_rw_SDLApp));

    ct_rw_init(app, title, width, height, renderFlags, windowFlags);

    return app;
}

void ct_rw_init(ct_rw_SDLApp *app, const char *title, int width, int height, int renderFlags, int windowFlags) {
    ct_ca_aNamePush("ct_rw_init");

    if (SDL_Init(SDL_INIT_VIDEO) < 0) {
        ct_ca_doAbort("SDL_Init failed: %s", SDL_GetError());
    }

    app->window = SDL_CreateWindow(title, SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, width, height, windowFlags);

    if (!app->window) {
        ct_ca_doAbort("Failed to SDL_CreateWindow (%ix%i): %s", width, height, SDL_GetError());
    }

    app->renderer = SDL_CreateRenderer(app->window, -1, renderFlags);

    if (!app->renderer) {
        ct_ca_doAbort("Failed to SDL_CreateRenderer: %s", SDL_GetError());
    }

    app->pixelBuffer = (struct ct_rb_RGBBitmap_s *) ct_rb_newRGB(width, height);

    ct_ca_aNamePop();
}

void ct_rw_renderBitmapRGB(ct_rw_SDLApp *app, ct_rb_RGBBitmap *bitmap) {
    ct_rw_clearColor(app);
    
    int ww, wh;
    SDL_GetWindowSize(app->window, &ww, &wh);

    for (long y = 0; y < wh; y++) {
        for (long x = 0; x < ww; x++) {
            // nearest neighbour interpolation
            long bx = ((double)x * bitmap->width  / ww);
            long by = ((double)y * bitmap->height / wh);

            // draw to window
            double *val = bitmap->vals + ((by * bitmap->width + bx) * 3);
            char rval_byte = val[0] * UCHAR_MAX;
            char gval_byte = val[1] * UCHAR_MAX;
            char bval_byte = val[2] * UCHAR_MAX;

            SDL_SetRenderDrawColor(app->renderer, rval_byte, gval_byte, bval_byte, 255);
            SDL_RenderDrawPoint(app->renderer, x, wh - y);
        }
    }
}

void ct_rw_renderBitmapG(ct_rw_SDLApp *app, ct_rb_DoubleBitmap *bitmap) {
    ct_rw_clearColor(app);

    int ww, wh;
    SDL_GetWindowSize(app->window, &ww, &wh);

    for (long y = 0; y < wh; y++) {
        for (long x = 0; x < ww; x++) {
            // nearest neighbour interpolation
            long bx = ((double)x * bitmap->width  / ww);
            long by = ((double)y * bitmap->height / wh);

            // draw to window
            double val = bitmap->vals[by * bitmap->width + bx];
            char val_byte = val * UCHAR_MAX;

            SDL_SetRenderDrawColor(app->renderer, val_byte, val_byte, val_byte, 255);
            SDL_RenderDrawPoint(app->renderer, x, wh - y);
        }
    }
}

void ct_rw_clearColor(ct_rw_SDLApp *app) {
    SDL_SetRenderDrawColor(app->renderer, 5, 5, 5, 255);
    SDL_RenderClear(app->renderer);
}

void ct_rw_rendererPresent(ct_rw_SDLApp *app) {
    SDL_RenderPresent(app->renderer);
}

void ct_rw_deinit(ct_rw_SDLApp *app) {
    SDL_DestroyRenderer(app->renderer);
    SDL_DestroyWindow(app->window);

    ct_rb_freeRGB(app->pixelBuffer);

    free(app);

    SDL_Quit();
}
