#ifndef HG_CAMERA
#define HG_CAMERA

#include "lib/common/quaternion.h"
#include "lib/common/vector.h"
#include "lib/common/triangle.h"



typedef struct {
    ct_cv_Vector pos;
    ct_cq_Quaternion rot;

    double fov;
} ct_rc_Camera;


void ct_rc_initCam(ct_rc_Camera *cam, ct_cv_Vector *pos, ct_cv_Vector *rotXYZ, double fov);
ct_rc_Camera *ct_rc_allocCam(ct_cv_Vector *pos, ct_cv_Vector *rotZYX, double fov);
void ct_rc_freeCam(ct_rc_Camera *heapCam);

void ct_rc_setFov(ct_rc_Camera *cam, double fov);

void ct_rc_rotateToXYZ(ct_rc_Camera *cam, ct_cv_Vector *rotZYX);
void ct_rc_rotateByXYZ(ct_rc_Camera *cam, ct_cv_Vector *rotZYX);
void ct_rc_rotateByQuat(ct_rc_Camera *cam, ct_cq_Quaternion *rotQuat);

void ct_rc_applyPerspective(ct_cv_Vector *targ, ct_rc_Camera *cam);
void ct_rc_makePerspectiveTriangle(ct_ct_Triangle2D *targ, ct_rc_Camera *cam, ct_ct_Triangle3D *tri);



#endif
