//! (WARNING: deprecated, please see quaternion.h and quaternion.c instead!)

#include <stdlib.h>
#include <assert.h>
#include <stdio.h>
#include <string.h>



//== MATRICES AND MATRIX OPERATIONS ==//

#include "matrix.h"
#include "fixed.h"


void ct_cm_free(ct_cm_Matrix *mat) {
    free(mat->values);
    free(mat);
}

ct_cm_Matrix *ct_cm_alloc(int rows, int cols) {
    long *values = malloc(sizeof(long) * rows * cols);
    ct_cm_Matrix *mat = malloc(sizeof(ct_cm_Matrix));

    mat->rows = rows;
    mat->cols = cols;
    mat->values = values;

    return mat;
}

void ct_cm_zero(ct_cm_Matrix *mat) {
    long *values = mat->values;
    int size = mat->rows * mat->cols;

    for (int i = 0; i < size; i++)
        values[i] = 0;
}

ct_cm_Matrix *ct_cm_new(int rows, int cols) {
    ct_cm_Matrix *mat = ct_cm_alloc(rows, cols);
    ct_cm_zero(mat);

    return mat;
}

ct_cm_Matrix *ct_cm_idNew(int dims) {
    ct_cm_Matrix *mat = ct_cm_alloc(dims, dims);

    long *values = mat->values;
    int size = mat->rows * mat->cols;

    for (int i = 0; i < size; i++)
        values[i] = i % dims == i / dims;

    return mat;
}

long ct_cm_get(ct_cm_Matrix *mat, int row, int col) {
    #ifdef DEBUG
    assert(row < mat->rows);
    assert(col < mat->cols);
    #endif
    
    return mat->values[row * mat->cols + col];
}

float ct_cm_getF(ct_cm_Matrix *mat, int row, int col) {
    return FXTOF(ct_cm_get(mat, row, col));
}

void ct_cm_set(ct_cm_Matrix *mat, int row, int col, long val) {
    #ifdef DEBUG
    assert(row < mat->rows);
    assert(col < mat->cols);
    #endif
    
    mat->values[row * mat->cols + col] = val;
}

ct_cm_Matrix *ct_cm_matMul(ct_cm_Matrix *A, ct_cm_Matrix *B) {
    assert(A->cols == B->rows);

    ct_cm_Matrix *res = ct_cm_new(A->rows, B->cols);

    for (int i = 0; i < A->rows; i++) {
        for (int j = 0; j < B->cols; j++) {
            long sum = 0;

            for (int k = 0; k < A->cols; k++) {
                sum += FXMUL(ct_cm_get(A, i, k), ct_cm_get(B, k, j));
            }
        
            ct_cm_set(res, i, j, sum);
        }
    }

    return res;
}

ct_cm_Matrix *ct_cm_elAdd(ct_cm_Matrix *A, ct_cm_Matrix *B) {
    #ifdef DEBUG
    assert(A->cols == B->cols);
    assert(A->rows == B->rows);
    #endif

    ct_cm_Matrix *res = ct_cm_new(A->rows, A->cols);

    for (int i = 0; i < A->rows; i++) {
        for (int j = 0; j < B->cols; j++) {
            ct_cm_set(res, i, j, ct_cm_get(A, i, j) + ct_cm_get(B, i, j));
        }
    }

    return res;
}

ct_cm_Matrix *ct_cm_elSub(ct_cm_Matrix *A, ct_cm_Matrix *B) {
    assert(A->cols == B->cols);
    assert(A->rows == B->rows);

    ct_cm_Matrix *res = ct_cm_new(A->rows, A->cols);

    for (int i = 0; i < A->rows; i++) {
        for (int j = 0; j < B->cols; j++) {
            ct_cm_set(res, i, j, ct_cm_get(A, i, j) - ct_cm_get(B, i, j));
        }
    }

    return res;
}

ct_cm_Matrix *ct_cm_elMul(ct_cm_Matrix *A, ct_cm_Matrix *B) {
    assert(A->cols == B->cols);
    assert(A->rows == B->rows);

    ct_cm_Matrix *res = ct_cm_new(A->rows, A->cols);

    for (int i = 0; i < A->rows; i++) {
        for (int j = 0; j < B->cols; j++) {
            ct_cm_set(res, i, j, FXMUL(ct_cm_get(A, i, j), ct_cm_get(B, i, j)));
        }
    }

    return res;
}


// Custom element-wise operations
typedef long (*ct_cm_elOper)(ct_cm_Matrix *A, ct_cm_Matrix *B, int row, int col, long Aval, long Bval);

long a_meo_add(ct_cm_Matrix *A, ct_cm_Matrix *B, int row, int col, long Aval, long Bval) {
    (void)(A), (void)(B), (void)(row), (void)(col);
    return Aval + Bval;
}

long a_meo_sub(ct_cm_Matrix *A, ct_cm_Matrix *B, int row, int col, long Aval, long Bval) {
    (void)(A), (void)(B), (void)(row), (void)(col);
    return Aval - Bval;
}

long a_meo_mul(ct_cm_Matrix *A, ct_cm_Matrix *B, int row, int col, long Aval, long Bval) {
    (void)(A), (void)(B), (void)(row), (void)(col);
    return FXMUL(Aval, Bval);
}

long a_meo_div(ct_cm_Matrix *A, ct_cm_Matrix *B, int row, int col, long Aval, long Bval) {
    (void)(A), (void)(B), (void)(row), (void)(col);
    return FXDIV(Aval, Bval);
}

long a_meo_invsub(ct_cm_Matrix *A, ct_cm_Matrix *B, int row, int col, long Aval, long Bval) {
    (void)(A), (void)(B), (void)(row), (void)(col); 
    return Bval - Aval;
}


ct_cm_Matrix *ct_cm_elApply(ct_cm_Matrix *A, ct_cm_Matrix *B, ct_cm_elOper oper) {
    assert(A->cols == B->cols);
    assert(A->rows == B->rows);

    ct_cm_Matrix *res = ct_cm_new(A->rows, A->cols);

    for (int i = 0; i < A->rows; i++) {
        for (int j = 0; j < B->cols; j++) {
            ct_cm_set(res, i, j, (*oper)(A, B, i, j, ct_cm_get(A, i, j), ct_cm_get(B, i, j)));
        }
    }

    return res;
}



#include "vector.h"

ct_cm_Matrix *ct_cm_vecToMat(ct_cv_Vector *vec) {
    ct_cm_Matrix *mat = ct_cm_new(1, 4);
    ct_cm_set(mat, 0, 0, vec->x);
    ct_cm_set(mat, 0, 1, vec->y);
    ct_cm_set(mat, 0, 2, vec->z);
    ct_cm_set(mat, 0, 3, 1.);

    return mat;
}

ct_cv_Vector *ct_cm_matToVec(ct_cm_Matrix *mat) {
    long w = ct_cm_get(mat, 0, 3);

    return ct_cv_vNew(
        FXDIV(ct_cm_get(mat, 0, 0), w),
        FXDIV(ct_cm_get(mat, 0, 1), w),
        FXDIV(ct_cm_get(mat, 0, 2), w)
    );
}

ct_cv_Vector *ct_cm_transform(ct_cv_Vector *in, ct_cm_Matrix *transform) {
    ct_cm_Matrix *m1 = ct_cm_vecToMat(in);

    ct_cm_Matrix *m2 = ct_cm_matMul(m1, transform);
    ct_cm_free(m1);

    ct_cv_Vector *res = ct_cm_matToVec(m2);
    ct_cm_free(m2);

    return res;
}

char *ct_cm_reprMat(ct_cm_Matrix *mat) {
    char* res = malloc(2048 * sizeof(char));
    char* resPtr = res;

    res[0] = '[';
    resPtr++;

    for (int i = 0; i < mat->rows; i++) {
        for (int j = 0; j < mat->cols; j++) {
            sprintf(resPtr, " %f ", FXTOF(ct_cm_get(mat, i, j)));
            resPtr += strlen(resPtr);
        }

        if (i < mat->rows - 1) {
            sprintf(resPtr, " | ");
            resPtr += strlen(resPtr);
        }
    }
    
    sprintf(resPtr, "]");

    if (!realloc(res, strlen(res))) return NULL;

    return res;
}


void ct_cm_printMat(ct_cm_Matrix *mat) {
    for (int i = 0; i < mat->rows; i++) {
        printf("[");

        for (int j = 0; j < mat->cols; j++) {
            printf(" %10f", FXTOF(ct_cm_get(mat, i, j)));
        }

        printf(" ]\n");
    }

    printf("\n");
}
