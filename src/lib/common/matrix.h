//! (WARNING: deprecated, please see quaternion.h and quaternion.c instead!)

#ifndef HG_MATRIX
#define HG_MATRIX


#include <stdlib.h>
#include <assert.h>



//== MATRICES AND MATRIX OPERATIONS ==//

typedef struct ct_cm_Matrix {
    int rows;
    int cols;
    long* values;
    
} ct_cm_Matrix;

ct_cm_Matrix *ct_cm_alloc(int rows, int cols);
void ct_cm_zero(ct_cm_Matrix *mat);
ct_cm_Matrix *ct_cm_new(int rows, int cols);
void ct_cm_free(ct_cm_Matrix *mat);

long ct_cm_get(ct_cm_Matrix *mat, int row, int col);
float ct_cm_getF(ct_cm_Matrix *mat, int row, int col);
void ct_cm_set(ct_cm_Matrix *mat, int row, int col, long val);

ct_cm_Matrix *ct_cm_matMul(ct_cm_Matrix *A, ct_cm_Matrix *B);
ct_cm_Matrix *ct_cm_elAdd(ct_cm_Matrix *A, ct_cm_Matrix *B);
ct_cm_Matrix *ct_cm_elSub(ct_cm_Matrix *A, ct_cm_Matrix *B);
ct_cm_Matrix *ct_cm_elMul(ct_cm_Matrix *A, ct_cm_Matrix *B);


// Custom element-wise operations
typedef long (*ct_cm_elOper)(ct_cm_Matrix *A, ct_cm_Matrix *B, int row, int col, long Aval, long Bval);

long a_meo_add(ct_cm_Matrix *A, ct_cm_Matrix *B, int row, int col, long Aval, long Bval);
long a_meo_sub(ct_cm_Matrix *A, ct_cm_Matrix *B, int row, int col, long Aval, long Bval);
long a_meo_mul(ct_cm_Matrix *A, ct_cm_Matrix *B, int row, int col, long Aval, long Bval);
long a_meo_div(ct_cm_Matrix *A, ct_cm_Matrix *B, int row, int col, long Aval, long Bval);
long a_meo_invsub(ct_cm_Matrix *A, ct_cm_Matrix *B, int row, int col, long Aval, long Bval);

ct_cm_Matrix *ct_cm_elApply(ct_cm_Matrix *A, ct_cm_Matrix *B, ct_cm_elOper oper);


char *ct_cm_reprMat(ct_cm_Matrix *mat);
void ct_cm_printMat(ct_cm_Matrix *mat);


//-- vectors as matrices --

#include "vector.h"

ct_cm_Matrix *ct_cm_vecToMat(ct_cv_Vector *vec);
ct_cv_Vector *ct_cm_matToVec(ct_cm_Matrix *mat);
ct_cv_Vector *ct_cm_transform(ct_cv_Vector *in, ct_cm_Matrix *transform);



#endif
