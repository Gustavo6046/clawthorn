#include <stdlib.h>

#include "triangle.h"



ct_ct_Triangle *ct_ct_make(ct_cv_Vector *a, ct_cv_Vector *b, ct_cv_Vector *c) {
    ct_ct_Triangle *tri = calloc(1, sizeof(ct_ct_Triangle));

    tri->a = *a;
    tri->b = *b;
    tri->c = *c;

    return tri;
}

void ct_ct_init(ct_ct_Triangle *tri, ct_cv_Vector a, ct_cv_Vector b, ct_cv_Vector c) {
    tri->a = a;
    tri->b = b;
    tri->c = c;
}

void ct_ct_free(ct_ct_Triangle *tri) {
    free(tri);
}
