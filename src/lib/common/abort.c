#include <stdio.h>
#include <string.h>
#include <stdarg.h>
#include <stdlib.h>



const char **aNameStack = NULL;
long aStackTop = -1;


char *ct_ca_getAName(void) {
    if (aNameStack == NULL || aStackTop == -1) return "";
    
    const char *s = aNameStack[aStackTop];
    char *res = malloc(strlen(s) + 4);

    sprintf(res, "[%s] ", s);

    return res;
}

void ct_ca_doAbort(char *msg, ...) {
    va_list args;
    va_start(args, msg);
    
    fprintf(stderr, "\n\n(!) %s", ct_ca_getAName());
    vfprintf(stderr, msg, args);
    fprintf(stderr, "\n");

    abort();
}

void ct_ca_aNamePush(const char *name) {
    if (aStackTop < 0) aStackTop = 0;
    else aStackTop++;

    aNameStack = realloc(aNameStack, sizeof(const char *) * (aStackTop + 1));

    if (!aNameStack) {
        fprintf(stderr, "\n\n<!!!> [ct_ca_aNamePush] Error reallocating abort name stack!\n");
        abort();
    }

    aNameStack[aStackTop] = name;
}

void ct_ca_aNamePop(void) {
    ct_ca_aNamePush("ct_ca_aNamePop");

    if (aStackTop == -1) {
        ct_ca_doAbort("Name stack already empty!");
    };

    aStackTop--;

    if (aStackTop > 0) {
        aNameStack = realloc(aNameStack, sizeof(const char *) * aStackTop);
    }

    else {
        free(aNameStack);
        aNameStack = NULL;
    }
}
