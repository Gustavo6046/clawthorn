#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <math.h>



//==== VECTORS ====

#include "fixed.h"
#include "abort.h"
#include "vector.h"




//--- Vector Manipulation ---

ct_cv_Vector *ct_cv_vZero() {
    return calloc(1, sizeof(ct_cv_Vector));
}

ct_cv_Vector *ct_cv_duplicate(ct_cv_Vector *from) {
    ct_cv_Vector *res = malloc(sizeof(ct_cv_Vector));
    
    for (int i = 0; i < 4; i++) {
        VIND(res, i) = VIND(from, i);
    }
    
    return res;
}

ct_cv_Vector *ct_cv_vAlloc() {
    return malloc(sizeof(ct_cv_Vector));
}

ct_cv_Vector *ct_cv_vNew(long x, long y, long z) {
    ct_cv_Vector *res = ct_cv_vAlloc();

    res->x = x;
    res->y = y;
    res->z = z;
    res->w = FX1P;
    
    return res;
}

ct_cv_Vector ct_cv_vVal(long x, long y, long z) {
    ct_cv_Vector res;

    res.x = x;
    res.y = y;
    res.z = z;
    res.w = FX1P;
    
    return res;
}

void ct_cv_vFree(ct_cv_Vector *v) {
    free(v);
}

ct_cv_Vector *ct_cv_add(ct_cv_Vector* a, ct_cv_Vector* b) {
    ct_cv_Vector *res = ct_cv_vAlloc();

    for (int i = 0; i < 4; i++) {
        VIND(res, i) = VIND(a, i) + VIND(b, i);
    }
    
    return res;
}

void ct_cv_setAdd(ct_cv_Vector* targ, ct_cv_Vector* a, ct_cv_Vector* b) {
    for (int i = 0; i < 4; i++) {
        VIND(targ, i) = VIND(a, i) + VIND(b, i);
    }
}

void ct_cv_addTo(ct_cv_Vector* targ, ct_cv_Vector* b) {
    for (int i = 0; i < 4; i++) {
        VIND(targ, i) += VIND(b, i);
    }
}

ct_cv_Vector *ct_cv_sub(ct_cv_Vector* a, ct_cv_Vector* b) {
    ct_cv_Vector *res = malloc(sizeof(ct_cv_Vector));
    
    for (int i = 0; i < 4; i++) {
        VIND(res, i) = VIND(a, i) - VIND(b, i);
    }
    
    return res;
}

void ct_cv_setSub(ct_cv_Vector* targ, ct_cv_Vector* a, ct_cv_Vector* b) {
    for (int i = 0; i < 4; i++) {
        VIND(targ, i) = VIND(a, i) - VIND(b, i);
    }
}

void ct_cv_subFrom(ct_cv_Vector* targ, ct_cv_Vector* b) {
    for (int i = 0; i < 4; i++) {
        VIND(targ, i) -= VIND(b, i);
    }
}

ct_cv_Vector *ct_cv_vMul(ct_cv_Vector* a, ct_cv_Vector* b) {
    ct_cv_Vector *res = malloc(sizeof(ct_cv_Vector));
    
    for (int i = 0; i < 4; i++) {
        VIND(res, i) = (VIND(a, i) >> 8) * (VIND(b, i) >> 8);
    }
    
    return res;
}

void ct_cv_vMulTo(ct_cv_Vector* targ, ct_cv_Vector* b) {
    for (int i = 0; i < 4; i++) {
        VIND(targ, i) = (VIND(targ, i) >> 8) * (VIND(b, i) >> 8);
    }
}

ct_cv_Vector *ct_cv_fMul(ct_cv_Vector* a, double b) {
    ct_cv_Vector *res = malloc(sizeof(ct_cv_Vector));
    
    for (int i = 0; i < 4; i++) {
        VIND(res, i) = VIND(a, i) * b;
    }
    
    return res;
}

void ct_cv_fMulTo(ct_cv_Vector* targ, double b) {
    for (int i = 0; i < 4; i++) {
        VIND(targ, i) *= b;
    }
}

ct_cv_Vector *ct_cv_vDiv(ct_cv_Vector* a, ct_cv_Vector* b) {
    ct_cv_Vector *res = malloc(sizeof(ct_cv_Vector));
    
    for (int i = 0; i < 4; i++) {
        VIND(res, i) = (VIND(a, i) << 8) / (VIND(b, i) >> 8);
    }
    
    return res;
}

void ct_cv_vDivTo(ct_cv_Vector* targ, ct_cv_Vector* b) {
    for (int i = 0; i < 4; i++) {
        VIND(targ, i) = (VIND(targ, i) << 8) / (VIND(b, i) >> 8);
    }
}

ct_cv_Vector *ct_cv_fDiv(ct_cv_Vector* a, double b) {
    ct_cv_Vector *res = malloc(sizeof(ct_cv_Vector));
    
    for (int i = 0; i < 4; i++) {
        VIND(res, i) = VIND(a, i) / b;
    }
    
    return res;
}

void ct_cv_fDivTo(ct_cv_Vector* targ, double b) {
    for (int i = 0; i < 4; i++) {
        VIND(targ, i) /= b;
    }
}

long ct_cv_vDot(ct_cv_Vector* a, ct_cv_Vector* b) {
    return a->x * b->x + a->y * b->y + a->z * b->z;
}

ct_cv_Vector *ct_cv_cross3(ct_cv_Vector *a, ct_cv_Vector *b) {
    ct_cv_Vector *res = malloc(sizeof(ct_cv_Vector));

    /*
    res->x = a->y * b->z - a->z * b->y;
    res->y = a->z * b->x - a->x * b->z;
    res->z = a->x * b->y - a->y * b->x;
    */

    for (int i = 0; i < 3; i++) {
        // generalized cross product, yey!
        VIND(res, i) = (
            VIND(a, (i + 1) % 3) * VIND(b, (i + 2) % 3) -
            VIND(b, (i + 1) % 3) * VIND(a, (i + 2) % 3)
        );
    }

    return res;
}

void ct_cv_cross3val(ct_cv_Vector *targ, ct_cv_Vector *a, ct_cv_Vector *b) {
    // for stack vectors and such
    for (int i = 0; i < 3; i++) {
        VIND(targ, i) = (
            VIND(a, (i + 1) % 3) * VIND(b, (i + 2) % 3) -
            VIND(b, (i + 1) % 3) * VIND(a, (i + 2) % 3)
        );
    }
}

long ct_cv_cross2(ct_cv_Vector *a, ct_cv_Vector *b) {
    // ignore Z components; do a 2D cross product, by
    // retrieving just the Z component of what would
    // be the result of the 3D cross product, effectively
    // using only the X and Y components of the input
    // vectors.
    
    return (a->x >> 8) * (b->y >> 8) - (a->y >> 8) * (b->x >> 8);
}

double ct_cv_cross2f(ct_cv_Vector *a, ct_cv_Vector *b) {
    // floating-point version of the above.

    return (double) (ct_cv_cross2(a, b)) / 65536;
}

char *ct_cv_vRepr(ct_cv_Vector *v) {
    char* res = malloc(300 * sizeof(char));

    sprintf(res, "(%10f, %10f, %10f)", FXTOF(v->x), FXTOF(v->y), FXTOF(v->z));
    if ((!(res = realloc(res, strlen(res) + 1))) || !strlen(res)) return NULL;

    return res;
}

long ct_cv_vSize(ct_cv_Vector *v) {
    return sqrt(ct_cv_vDot(v, v));
}
